# Gitlab Flavored Markdown(GFM)说明文档
#### by glongzh

---

Gitlab Flavored Markdown受[GitHub Flavored Markdown](https://help.github.com/articles/basic-writing-and-formatting-syntax/)的影响，并在[CommonMark][https://github.com/gjtorikian/commonmarker]的基础上扩展了一些很实用的功能。

Gitlab可以在以下这些地方使用GFM：

* comments
* issues
* merge requests
* milestones
* snippets 
* wiki
* 仓库内的md文档

---

## 基本语法

---

### 标题

```no-highlight
# H1
## H2
### H3
#### H4
##### H5
###### H6
```

---

### 列表

```no-highlight
1. 有序列表的item
2. 这是另一个item
   * 无序子列表
1. 具体数字并不关心，只要数字就行
   1. 有序子列表
4. 这是另一个item

* 无序列表可以用星号
- 或减号
+ 或加号
```

结果：

1. 有序列表的item
2. 这是另一个item
   * 无序子列表
3. 具体数字并不关心，只要数字就行
   1. 有序子列表
4. 这是另一个item

* 无序列表可以用星号
- 或减号

+ 或加号
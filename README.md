1. Productpage发起对Details的调用：`http://details:9080/details/0` 。

2. 请求被Pod的iptable规则拦截，转发到15001端口。

3. Envoy监听15001端口收到了该请求。由于该Listener的`use_original_dst`配置为`true`，所以该Listener的所有请求都会转发到请求的原始目标端口。这也是为什么将该Listener命名为virtual的原因。

   > 转发是根据目标IP+端口转发的，未匹配到端口的请求会被转到BlackHoleCluster（即丢弃）；匹配到端口未匹配到IP的，则转发到通配Listener(即0.0.0.0_port的Listener)。

4. 请求被Virtual Listener根据原目标IP（通配）和端口（9080）转发到0.0.0.0_9080这个listener。

5. 根据0.0.0.0_9080 listener的配置的Filter是http_connection_manager, 从该filter配置的`rds`字段可以知道，该请求采用“9080” route进行分发。

6. “9080”这个route的配置中，根据host name可以在`virtual_hosts`中找到details:9080的请求对应的cluster为outbound|9080||details.default.svc.cluster.local

7. outbound|9080||details.default.svc.cluster.local cluster为动态资源，通过eds查询得到其endpoint为192.168.206.21:9080。

8. 请求被转发到192.168.206.21，即Details服务所在的Pod，被iptable规则拦截，转发到15001端口。

9. Envoy的Virtual Listener在15001端口上监听，收到了该请求。

10. 请求被Virtual Listener根据请求原目标地址IP（192.168.206.21）和端口（9080）转发到192.168.206.21_9080这个listener。

11. 根据92.168.206.21_9080 listener的http_connection_manager filter配置,该请求对应的cluster为 inbound|9080||details.default.svc.cluster.local 。

12. inbound|9080||details.default.svc.cluster.local cluster配置的host为s127.0.0.1：9080。

13. 请求被转发到127.0.0.1：9080，即Details服务进行处理。


